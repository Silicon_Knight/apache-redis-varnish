#!/usr/bin/env bash
###Variables###
wpconfig=/var/www/html/wp-config.php
###Variables###


###Wordpress with Varnish on Apache2
apt-get update && apt-get upgrade -y
apt-get install wget unzip htop -y

# S3CMD for testing? #
apt-get install s3cmd -y
wget "https://drive.google.com/uc?export=download&id=0B1kr44Yp5RoHZ2xYS1NqNlJuUXc" -O /home/$(whoami)/.s3cfg
# S3CMD for testing? #

# Apache2
apt install -y apache2 php7.0 php7.0-mysql libapache2-mod-php php7.0-curl php7.0-tidy php7.0-mbstring php7.0-gd php7.0-xml php-redis
a2enmod expires headers ext_filter rewrite deflate
mv configs/ports.conf /etc/apache2/ports.conf
mv configs/000-default.conf /etc/apache2/sites-enabled/000-default.conf
mv configs/apache2.conf /etc/apache2/apache2.conf
systemctl restart apache2.service

# Setup PHP
mv configs/php.ini /etc/php/7.0/apache2/php.ini

# Varnish
apt install -y varnish
mv configs/varnish.service /lib/systemd/system/varnish.service
mv configs/varnish /etc/default/varnish
systemctl daemon-reload
systemctl restart varnish.service

# Install Wordpress
rm -r /var/www/html/*
wget https://wordpress.org/latest.tar.gz -P /home/$(whoami)/
tar -C /var/www/html/ -xzvf /home/$(whoami)/latest.tar.gz
mv /var/www/html/wordpress/* /var/www/html/
mv configs/wp-config.php /var/www/html/wp-config.php
mv configs/.htaccess /var/www/html/.htaccess
wget https://assets.digitalocean.com/articles/wordpress_redis/object-cache.php -O /var/www/html/wp-content/object-cache.php
mv creds.php /var/www/html/creds.php

# Remove install.php SO YOU HAVE TO INSTALL MANUALLY!!! #
rm /var/www/html/wp-admin/install.php 
# WP Security Stuff
sed -i -e "s/define('AUTH_KEY','');/define('AUTH_KEY','$(cat /dev/urandom | tr -cd 'a-z0-9A-Z@#$%^*()_+:?.,<>' | head -c 64)');/g" $wpconfig
sed -i -e "s/define('SECURE_AUTH_KEY','');/define('SECURE_AUTH_KEY','  $(cat /dev/urandom | tr -cd 'a-z0-9A-Z@#$%^*()_+:?.,<>' | head -c 64)');/g" $wpconfig
sed -i -e "s/define('LOGGED_IN_KEY','');/define('LOGGED_IN_KEY','$(cat /dev/urandom | tr -cd 'a-z0-9A-Z@#$%^*()_+:?.,<>' | head -c 64)');/g" $wpconfig
sed -i -e "s/define('NONCE_KEY','');/define('NONCE_KEY','$(cat /dev/urandom | tr -cd 'a-z0-9A-Z@#$%^*()_+:?.,<>' | head -c 64)');/g" $wpconfig
sed -i -e "s/define('AUTH_SALT','');/define('AUTH_SALT','$(cat /dev/urandom | tr -cd 'a-z0-9A-Z@#$%^*()_+:?.,<>' | head -c 64)');/g" $wpconfig
sed -i -e "s/define('SECURE_AUTH_SALT','');/define('SECURE_AUTH_SALT','$(cat /dev/urandom | tr -cd 'a-z0-9A-Z@#$%^*()_+:?.,<>' | head -c 64)');/g" $wpconfig
sed -i -e "s/define('LOGGED_IN_SALT','');/define('LOGGED_IN_SALT','$(cat /dev/urandom | tr -cd 'a-z0-9A-Z@#$%^*()_+:?.,<>' | head -c 64)');/g" $wpconfig
sed -i -e "s/define('NONCE_SALT','');/define('NONCE_SALT','$(cat /dev/urandom | tr -cd 'a-z0-9A-Z@#$%^*()_+:?.,<>' | head -c 64)');/g" $wpconfig
# WP Security Stuff

# Grab themes and plugins
wget https://downloads.wordpress.org/theme/button.1.0.4.zip -O ~/button.1.0.4.zip
unzip ~/button.1.0.4.zip -d /var/www/html/wp-content/themes/
wget https://downloads.wordpress.org/plugin/js-css-script-optimizer.zip -O ~/js-css-script-optimizer.zip 
unzip ~/js-css-script-optimizer.zip -d /var/www/html/wp-content/plugins/
wget https://downloads.wordpress.org/plugin/autodescription.2.9.4.zip -O ~/seo.zip
unzip ~/seo.zip -d /var/www/html/wp-content/plugins/
wget https://downloads.wordpress.org/plugin/amazon-web-services.1.0.3.zip -O ~/aws.zip
unzip ~/aws.zip -d /var/www/html/wp-content/plugins/
wget https://downloads.wordpress.org/plugin/amazon-s3-and-cloudfront.1.2.zip -O ~/s3.zip
unzip ~/s3.zip -d /var/www/html/wp-content/plugins/
wget https://downloads.wordpress.org/plugin/ml-slider.3.5.1.zip -O ~/slider.zip
unzip ~/slider.zip -d /var/www/html/wp-content/plugins/

# Grab themes and plugins

# Install Redis
apt-get -y install redis-server
su -c "echo 'maxmemory 1024mb' >> /etc/redis/redis.conf"
su -c "echo 'maxmemory-policy allkeys-lru' >> /etc/redis/redis.conf"

# Configure Redis for Wordpress
# wget https://assets.digitalocean.com/articles/wordpress_redis/object-cache.php -O /var/www/html/wp-content/object-cache.php

# Mount photos for wordpress
mv configs/fstab /etc/fstab
mkdir /var/www/html/wp-content/uploads
mount -a

# Clean everything up
rm -r /var/www/html/wordpress/ #Clean wordpress folder from unzip
rm -R /home/$(whoami)/* #Clean the home directory
chown -R www-data:www-data /var/www/ #CHOWN everything

service apache2 stop
service varnish stop
service redis stop
service apache2 start
service varnish start
service redis start

